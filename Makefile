#
# make deploy-test - run kustomize and oc apply on all test environments.
# make deploy-prod - run kustomize and oc apply on prod.
#
#
.ONESHELL:


## Determine which cluster we are logged in to
CLUSTER = $(shell env TERM=dumb LANG=C oc status 2>/dev/null | grep "on server" | sed 's|^.*on server ||')
ifneq ($(CLUSTER), "")
$(info Operating on $(CLUSTER).)
else
$(error You must be logged in to an OpenShift cluster.)
endif

## Determine which git branch is currently in use
BRANCH  = $(shell LANG=C git rev-parse --symbolic-full-name --abbrev-ref HEAD)

TESTCLUSTER = "https://api.test.services.jtech.se:6443"
PRODCLUSTER = "https://api.prod.services.jtech.se:6443"

## The name of the directory containing the prod configuration
ATLASPRODENVDIR = k8s/atlas/prod
EDITORPRODENVDIR = k8s/editor/prod

## This env is handled by tekton/pipeline-taxonomy-editor.yaml and deployed via webhooks.
ATLASDEVENVDIR = k8s/atlas/dev
EDITORDEVENVDIR = k8s/editor/dev
ANNOTATIONEDITORDEVENVDIR = k8s/annotation-editor/dev
LANDINGPAGEDEVENVDIR = k8s/landingpage/dev

## The directories containing the test environment configurations
ATLASTESTENVDIR = k8s/atlas/test
EDITORTESTENVDIR = k8s/editor/test
ANNOTATIONEDITORTESTENVDIR = k8s/annotation-editor/test


PROJECT = taxonomy-frontend-gitops

# Usage: make set-api-image SHA=35fe8ad53bad8358f774cf557e1ce4c799238315
# Sets the deployment image for test
set-api-image:
	@if [ -z "$(SHA)" ]; then
		echo "**** error: env variable SHA not set"
		exit 1
	else
		sed -i.old "s/gitops:.*/gitops:$(SHA)/" k8s/test/set-deployment-image-taxonomy-editor.yaml
	fi

promote-from-test:
	cp k8s/test/set-deployment-image-taxonomy-editor.yaml k8s/prod/

login-test:
	oc login https://api.test.services.jtech.se:6443 --password='' --username=''

login-prod:
	oc login https://api.prod.services.jtech.se:6443 --password='' --username=''

apply-dev: check-logged-in gitcheck check-secrets check-project
	@if [ "$(CLUSTER)" != "$(TESTCLUSTER)" ]; then
		echo "**** error: not logged in to $(TESTCLUSTER)  $(CLUSTER)"
		exit 1
	fi
	kustomize build $(ATLASDEVENVDIR) | oc apply -f -
	kustomize build $(EDITORDEVENVDIR) | oc apply -f -
	kustomize build $(ANNOTATIONEDITORDEVENVDIR) | oc apply -f -
	kustomize build $(LANDINGPAGEDEVENVDIR) | oc apply -f -


deploy-test: check-logged-in gitcheck check-secrets setup-test
	@if [ "$(CLUSTER)" != "$(TESTCLUSTER)" ]; then
		echo "**** error: not logged in to $(TESTCLUSTER)  $(CLUSTER)"
		exit 1
	fi
	kustomize build $(ATLASTESTENVDIR) | oc apply -f -
	kustomize build $(EDITORTESTENVDIR) | oc apply -f -
	kustomize build $(ANNOTATIONEDITORTESTENVDIR) | oc apply -f -


deploy-prod: check-logged-in gitcheck check-secrets setup-prod
	@if [ "$(CLUSTER)" != "$(PRODCLUSTER)" ]; then
		echo "**** error: not logged in to $(PRODCLUSTER)"
		exit 1
	fi
	kustomize build $(ATLASPRODENVDIR) | oc apply -f -
	kustomize build $(EDITORPRODENVDIR) | oc apply -f -


#### Helper targets - not to be run from command line directly #####

setup-test: check-project
	oc apply -f config/editor/test/taxonomy-editor-test.yaml
	oc apply -f config/atlas/test/taxonomy-atlas-test.yaml
	oc apply -f config/service-accounts.yaml
	oc apply -f tekton/build-pipeline.yaml
	oc apply -f jobtech-taxonomy-api-deploy-secrets/nexus/nexus.yaml

setup-prod: check-project
	oc apply -f config/editor/prod/taxonomy-editor-prod.yaml
	oc apply -f config/atlas/prod/taxonomy-atlas-prod.yaml
	oc apply -f config/service-accounts.yaml
	oc apply -f jobtech-taxonomy-api-deploy-secrets/nexus/nexus.yaml

check-project:
	oc new-project $(PROJECT) || oc project $(PROJECT)
	@if [ "$$(oc project -q)" != "$(PROJECT)" ]; then
		echo "**** error: failed to switch to $(PROJECT)"
		exit 1
	fi

gitcheck:
	@if [ ! -z "$$(git status --porcelain)" ]; then
		echo "**** error: repo is not clean"
		exit 1
	fi
	if [ "$(BRANCH)" != "master" ]; then
		echo "**** error: not on master ($(BRANCH))"
		exit 1
	fi
	git pull

check-logged-in:
	if [ -z "$(CLUSTER)" ]; then
		exit 1
	fi

check-secrets:
	@if [ ! -d "jobtech-taxonomy-api-deploy-secrets" ]; then
		echo "**** error: secrets are missing"
		exit 1
	fi
