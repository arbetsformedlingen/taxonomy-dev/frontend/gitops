## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)



## About The Project

A take on a declarative build and deploy setup for the [Jobtech Taxonomy Fronted](https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend).

It is built for OpenShift 4.3.


### Built With

* [oc - the OpenShift command line](https://github.com/openshift/oc)
* [Kustomize](https://github.com/kubernetes-sigs/kustomize)

## Getting Started

### Prerequisites

Install the `oc` and `kustomize` programs.

### Installation

Clone this repo.

Then get the submodule: `git submodule update --init --recursive`

That submodule contains a root kustomization.yaml with the following content:
```
generatorOptions:
 disableNameSuffixHash: true

secretGenerator:
- name: jobtech-taxonomy-api-secrets
  envs:
  - secrets/api-secrets-test.txt
  - secrets/aws-secrets-test.txt
```

## Usage

Begin any session by logging in to the correct OpenShift cluster.

Deploys are made using `make`. Check the Makefile for available targets.

FIXME: finish writing this usage guide, to describe the complete workflow.

## Roadmap

See the [open issues](https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/gitops/-/issues) for a list of proposed features (and known issues).

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request


## License

Distributed under the GNU GPL v3 License. See `LICENSE` for more information.


## Contact

[jobtechdev@arbetsformedlingen.se](mailto:jobtechdev@arbetsformedlingen.se)

Project Link: [https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/gitops](https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/gitops)
